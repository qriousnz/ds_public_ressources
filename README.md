This repository contains public ressources. 

A ressource can be accessed with this path https://bitbucket.org/qriousnz/ds_public_ressources/raw/master/<ressource-file-name>

Content:

- geospatial
  - [geospatial/au_centroids.geojson](geospatial/au_centroids.geojson) - Area of unit centroids
